import React, { Component } from 'react';
import { View, ScrollView, ImageBackground, Image, StyleSheet, Text, TouchableHighlight } from 'react-native';

export default function AppPage2(props) {
  return (

    <ImageBackground source={{ uri: 'https://i.pinimg.com/564x/ff/6d/14/ff6d14f54adfd1c75d0070ec8079a6d7.jpg' }} style={{ width: '100%', height: '100%' }}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          zIndex: 1
        }}>
        <View
          style={{
            backgroundColor: '#6300EE',
            height: 50,
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{ width: 20, height: 20 }}
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/130/130918.png',
            }}>
          </Image>
        </View>
        <View
          style={{
            backgroundColor: '#03DAC5',
            height: 50,
            flex: 5,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>AppName</Text>
        </View>
        <View
          style={{
            backgroundColor: '#03DAC5',
            height: 50,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{ width: 20, height: 20 }}
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/93/93642.png',
            }}>
          </Image>
        </View>
        <View
          style={{
            backgroundColor: '#6100EE',
            height: 50,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableHighlight
            onPress={() => props.navigation.goBack()}
          >
            <Image
              style={{ width: 20, height: 20 }}
              source={{
                uri: 'https://image.flaticon.com/icons/png/512/126/126467.png',
              }}>
            </Image>
          </TouchableHighlight>
        </View>
      </View>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={{ flex: 1, zIndex: 0 }} >
          <View
            style={{
              backgroundColor: 'skyblue',
              height: 400,
              flex: 5,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: '5%',
              marginRight: '5%',
              marginTop: '10%',
              marginBottom: '10%'
            }}>
            <View>
              <Text>
                UserName:{props.route.params.inputname}
              </Text>
            </View>
            <View>
              <Text>
                Password:{props.route.params.inputpassword}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
          zIndex: 1
        }}>
        <View
          style={{
            backgroundColor: '#03DAC5',
            height: 50,
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{ width: 20, height: 20 }}
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/90/90003.png',
            }}>
          </Image>
        </View>
        <View
          style={styles.iconBotton}>
          <Image
            style={{ width: 20, height: 20 }}
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/88/88179.png',
            }}>
          </Image>
        </View>
        <View
          style={styles.iconBotton}>
          <Image
            style={{ width: 20, height: 20 }}
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/13/13787.png',
            }}>
          </Image>
        </View>
        <View
          style={styles.iconBotton}>
          <Image
            style={{ width: 20, height: 20 }}
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/19/19925.png',
            }}>
          </Image>
        </View>
      </View>
    </ImageBackground>

  );
}

const styles = StyleSheet.create({
  iconBotton: {
    backgroundColor: '#03DAC5',
    height: 50,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }
});


