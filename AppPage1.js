import React, {Component, useState} from 'react';
import {
  View,
  ScrollView,
  ImageBackground,
  Image,
  TextInput,
  StyleSheet,
  Text,
  TouchableHighlight,
} from 'react-native';

export default function AppPage1() {
  const [inputname,setInputname]=useState('');
  const [inputpassword,setInputpassword]=useState('');

  setTextname = text => {
    setInputname(text);
  };

  setTextpass = textPass => {
    setInputpassword(textPass);
  };

    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={{flex: 1}}>
          <ImageBackground
            source={{
              uri:
                'https://i.pinimg.com/564x/ff/6d/14/ff6d14f54adfd1c75d0070ec8079a6d7.jpg',
            }}
            style={{width: '100%', height: '100%'}}>
            <View
              style={{
                height: 100,
                flex: 2,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: '5%',
                marginRight: '5%',
                marginTop: '10%',
              }}>
              <Image
                style={{width: 150, height: 150}}
                source={{
                  uri: 'https://image.flaticon.com/icons/png/512/94/94405.png',
                }}></Image>
            </View>
            <View style={styles.ButtonInput}>
              <TextInput
                style={{
                  height: 40,
                  width: 200,
                  borderColor: 'gray',
                  borderWidth: 1,
                  borderRadius: 5,
                }}
                placeholder="Username"
                onChangeText={text => setTextname(text)}
              />
            </View>
            <View style={styles.ButtonInput}>
              <TextInput
                style={{
                  height: 40,
                  width: 200,
                  borderColor: 'gray',
                  borderWidth: 1,
                  borderRadius: 5,
                }}
                placeholder="Password"
                onChangeText={textPass => setTextpass(textPass)}
              />
            </View>
            <View style={styles.ButtonInput}>
              <TouchableHighlight
                style={styles.button}
                onPress ={()=> props.navigation.navigate('AppPage2',{
                  inputname:inputname,
                  inputpassword:inputpassword
                })}>
                <Text>Login</Text>
              </TouchableHighlight>
            </View>
            <View
              style={{
                height: 100,
                flex: 6,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }

const styles = StyleSheet.create({
  ButtonInput: {
    height: 50,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
    marginTop: '5%',
    marginRight: '5%',
    elevation:8
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    elevation:8
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    elevation:8
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
    elevation:8
  },
  countText: {
    color: '#FF00FF',
    elevation:8
  },
});
