import {NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react'
import Main from './Main'
import AppPage1 from './AppPage1';

import Test1 from './Test1';
import Memin from './Memin';

const Tab = createBottomTabNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Memin" component={Memin} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
